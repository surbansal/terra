variable "project" {
    type = string
}

variable "account_id" {
    type = string
    description = "For the SA"
}

variable "name" {
    type = string
    description = "Cluster name"
}

variable "location" {
    type = string
    description = "Cluster location"
    default = "us-central1"
}
