resource "google_service_account" "default" {
  account_id   = var.account_id
  display_name = "Service Account"
  project = var.project
}

resource "google_container_cluster" "primary" {
  provider                    = google-beta
  name     = var.name
  location = var.location
  project = var.project

  # We can't create a cluster with no node pool defined, but we want to only use
  # separately managed node pools. So we create the smallest possible default
  # node pool and immediately delete it.
  remove_default_node_pool = true
  initial_node_count       = 1
  enable_binary_authorization = true
  network    = google_compute_network.ips-dev-vpc.name
  subnetwork = google_compute_subnetwork.ips-subnet-uscentral1.name

  release_channel {
    channel = "REGULAR"
  }

  ip_allocation_policy {
    cluster_secondary_range_name  = "gke-pods"
    services_secondary_range_name = "gke-services"
  }
  
  master_authorized_networks_config {
    cidr_blocks {
      display_name = "Anywhere"
      cidr_block   = "0.0.0.0/0"
    }
  }
  
  networking_mode = "VPC_NATIVE"
  
  private_cluster_config {
    enable_private_endpoint = false
    enable_private_nodes    = true
    master_ipv4_cidr_block  = "172.16.0.32/28"
  }
  depends_on = [google_compute_router_nat.ips-nat]
}

resource "google_container_node_pool" "primary_preemptible_nodes" {
  name       = "my-node-pool"
  location   = var.location
  cluster    = google_container_cluster.primary.name
  node_count = 2
  project = var.project
  node_config {
    preemptible  = false
    machine_type = "e2-medium"

    # Google recommends custom service accounts that have cloud-platform scope and permissions granted via IAM Roles.
    service_account = google_service_account.default.email
    oauth_scopes    = [
      "https://www.googleapis.com/auth/cloud-platform"
    ]
  }
}