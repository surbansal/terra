
resource "google_compute_network" "ips-dev-vpc" {
  name                    = "ips-dev-vpc"
  auto_create_subnetworks = false
  project = var.project
}

resource "google_compute_subnetwork" "ips-subnet-uscentral1" {
  name                     = "ips-dev-vpc-uscentral1"
  ip_cidr_range            = "10.0.0.0/24"
  region                   = "us-central1"
  network                  = google_compute_network.ips-dev-vpc.self_link
  project = var.project
  private_ip_google_access = "true"
  secondary_ip_range {
    range_name    = "gke-services"
    ip_cidr_range = "10.105.0.0/22"
  }
  secondary_ip_range {
    range_name    = "gke-pods"
    ip_cidr_range = "10.104.0.0/16"
  }
}

resource "google_compute_router" "ips-router" {
  name    = "ips-vpc-cloud-router"
  network = google_compute_network.ips-dev-vpc.id
  project = var.project
  region  = var.location
}

resource "google_compute_router_nat" "ips-nat" {
  name                               = "ips-cloud-router-nat"
  router                             = google_compute_router.ips-router.name
  nat_ip_allocate_option             = "MANUAL_ONLY"
  nat_ips                            = [google_compute_address.cloudnat.self_link]
  source_subnetwork_ip_ranges_to_nat = "ALL_SUBNETWORKS_ALL_IP_RANGES"
  project = var.project
  region                             = var.location
  log_config {
    enable = true
    filter = "ERRORS_ONLY"
  }
}

resource "google_compute_address" "cloudnat" {
  project = var.project
  name    = "cloudnat-${var.project}"
  region  = var.location
}
