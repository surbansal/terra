resource "google_composer_environment" "composer" {
  project = var.project_id

  name   = "cwow-composer"
  region = var.composer_region

  config {
    node_count = var.composer_node_count

    private_environment_config {
      enable_private_endpoint = var.enable_private_endpoint
      master_ipv4_cidr_block  = "10.82.4.128/28"
    }

    node_config {
      zone         = var.composer_node_zone
      machine_type = var.composer_node_type
      disk_size_gb = 20
      network      = "default"
      subnetwork   = "us-central1"

      service_account = google_service_account.composer_service_account.name

      ip_allocation_policy {
        use_ip_aliases                = true
        cluster_secondary_range_name  = "composer-pods"
        services_secondary_range_name = "composer-services"
      }
    }

    software_config {
      airflow_config_overrides = {
        secrets-backend = "airflow.providers.google.cloud.secrets.secret_manager.CloudSecretManagerBackend"
      }
      image_version  = "composer-1.14.4-airflow-1.10.12"
      python_version = "3"
    }
  }

  labels = {
    env     = var.env
    service = "composer"
  }
}

resource "google_service_account" "composer_service_account" {
  project      = "surabhi-terraform"
  account_id   = "composer-service-account"
  display_name = "Composer Service Account for Composer Environment"
}
