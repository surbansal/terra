variable "env" {
  description = "composer environment"
  type        = string
  default     = "np"
}

variable "project_id" {
    description = "project"
    type = string
}

variable "composer_region" {
  description = "gcp region for composer"
  type        = string
}

variable "composer_node_count" {
  description = "number of the composer nodes"
  type        = string
  default  = "3"
}

variable "composer_node_type" {
  description = "gce instance type for composer nodes"
  type        = string
  default     = "n1-standard-2"
}

variable "composer_node_zone" {
  description = "GCP zone of composer nodes"
  type        = string
  default = "us-central1-f"
}

variable "composer_gcp_connection_scope" {
  description = "GCP scope to be used in the google secret manager"
  type        = string
  default     = "https://www.googleapis.com/auth/cloud-platform"
}

variable "enable_private_endpoint" {
  description = "Access to the public endpoint of the GKE cluster is denied"
  type        = string
  default     = "true"
}